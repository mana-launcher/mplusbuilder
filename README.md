# MPlusBuilder

This repo provides a pipeline for building and packaging manaplus to an AppImage for use with 
our [launcher](https://gitlab.com/TMW2/lawncable).

**IMPORTANT:** The resulting appimage doesn't support relative paths in its arguments. SO ONLY USE **ABSOLUTE** PATHS.

### Special Compilation Options

```
--enable-commandlinepassword
```